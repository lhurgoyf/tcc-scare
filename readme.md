## Make sure you have apache2 installed

```
$ sudo apt-get install apache2
```

## Make sure you have PHP 5.* installed

```
$ sudo apt-get install php5
```

## You MUST install composer
How to? [Follow instructions](https://getcomposer.org/download/)

## Run composer
Go to /your/project/folder/lummin

```
$ composer install
```
## Install mysql
Once you run composer, make sure you have mysql-server-5.* installed, run: 

```
$ sudo apt-get install mysql-server-5.6
```