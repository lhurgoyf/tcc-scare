$(document).ready(function(){
    $('.date').mask('99/99/9999');
    $('.time').mask('99:99');
    $('.cpf').mask('000.000.000.00');
    $('.money').mask('000.000.00', {reverse: true});
    $('.phone_with_ddd').mask('(00) 0000-0000');
    $('.alert').delay(3000).fadeOut();
    $('.cep').mask('00.000-00');
	});
