@extends('layouts.template')
@section('content')
<script>
	$(document).ready(function() {
		$('#calendar').fullCalendar({

			dayClick: function(date) 

        		{
          			var getDate = date.format('DD/MM/YYYY')
          			$('.date').val(getDate)
          			$('.day-click').modal('show');

        		},
        		
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			eventLimit: false,	
			eventClick: function (data) {
				var id = data.event_id
				$.ajax({
					type: 'GET',	
					url: '/agenda/' + id,
					dataType: 'JSON',
					success: function(res) {
						var date = moment(res.date).format('DD/MM/YYYY')
						var time = moment(res.date).format('HH:mm')
						$('#customer-' + res.id_cliente).attr('selected', true)
						$('#profess-' + res.profissional).attr('selected', true)
						$('.date').val(date)
						$('.time').val(time)
						$('.select').trigger("change");
						$('.day-clickUpdate').modal('show');
						$('#agenda_id').val(id);
						$('#delete-event').attr('href', '/agenda/delete/' + id);
						$('.sl-update').attr('selected', false)
						$.each(res.services, function(index, obj){
							$('#serv-' + obj.id).attr('selected', true)
							$('.select').trigger('change')
						})
					}
				})
			},
			events: [
				<?php foreach($agendas as $agenda) : ?>
					<?php 
						$customer = unserialize($agenda->data);
						$nome = $customer['nome'];
					?>
					{
						event_id: '<?=$agenda->agenda_id?>',
						title: '<?=$nome?>',
						start: '<?=$agenda->date?>',
					},
				<?php endforeach; ?>	
				]
		});
		$('.select').select2({width:'60%'})
	});
	
</script>
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>
		</div> <!--/.row-->
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div id="calendar"></div>
						  <div class="modal fade day-click" role="dialog">
						    <div class="modal-dialog">
						      <div class="modal-content">
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal" data-target="modal" aria-label="Clse">
						          <span aria-hidden="true">&times;</span></button>
						          <h4 class="modal-title">Marcar Horario</h4>
						        </div>
							        <div class="modal-body">
							          <form action="/agenda" method="POST">
							          	  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
								          <div class="form-group label-floating">
								            <label class="control-label">Cliente:</label>
								            <select name="id_cliente" class="select form-control">
								            	@foreach($customers as $custom)
							            		<option  value="{{$custom['id']}}">{{$custom['data']['nome']}}</option>
								            	@endforeach
								            </select>
								          </div>
											<div class="form-group label-floating 12">
								            <label class="control-label">Serviço:</label>
									          <select multiple required="required" name="servico[]" class="select form-control">
									            	@foreach($services as $som)
								            		<option value="{{$som['id']}}">{{$som['tipo']}}</option>
									            	@endforeach
									            </select>
									          </div>
								          <div class="form-group">
								            <label for="recipient-name" class="control-label">Data:</label>
								            <input type="text" name="date" required class="date form-control">
									      </div>
								           <div class="form-group">
								            <label for="recipient-name" class="control-label">Horario:</label>
								            <input type="text" required name="time" class="time form-control">
								          </div>
								          <label class="control-label">Profissional:</label>
									          <select  name="profissional" class="select form-control">
									            	@foreach($profess as $prof)
								            		<option value="{{$prof['id']}}">{{$prof['data']['nome']}}</option>
									            	@endforeach
									            </select>
									          </div>
							          <div class="modal-footer">
							             <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
							             <button type="submit" class="btn btn-info">Salvar</button>
							          </div>
							        </form>
							      </div> <!-- /.modal-content -->
							    </div> <!-- /.modal-dialog -->
							  </div> <!-- /.modal -->
							</div>
						</div>
					</div>
				</div>
			</div><!--/.row-->	
			<div class="modal fade day-clickUpdate" role="dialog">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" data-target="modal" aria-label="Clse">
			          <span aria-hidden="true">&times;</span></button>
			          <h4 class="modal-title">Editar Marcação de Horário</h4>
			        </div>
				        <div class="modal-body">
			          	  <a href="#" id="delete-event" class="btn btn-danger pull-right">Excluir</a>		        
				          <form action="/agenda/update/data" method="POST">
				          	  <input type="hidden" name="_token" value="{{ csrf_token() }}" />
				          	  <input type="hidden" id="agenda_id" name="id" />
					          <div class="form-group label-floating">
					            <label class="control-label">Cliente:</label>
					            <select type="text" required="required" name="id_cliente" id="customers" class="select form-control">
					            	@foreach($customers as $custom)
				            		<option id="customer-{{$custom['id']}}" value="{{$custom['id']}}">{{$custom['data']['nome']}}</option>
					            	@endforeach
					            </select>
					          </div>
								<div class="form-group label-floating 12">
					            <label class="control-label">Serviço:</label>
						          <select type="text" required="required" multiple name="servicos[]" class="select form-control">
						            	@foreach($services as $som)
					            		<option id="serv-{{$som['id']}}" class="sl-update" value="{{$som['id']}}">{{$som['tipo']}}</option>
						            	@endforeach
						            </select>
						          </div>
					          <div class="form-group">
					            <label for="recipient-name" class="control-label">Data:</label>
					            <input type="text" name="date" required class="date form-control">
						      </div>
					           <div class="form-group">
					            <label for="recipient-name" class="control-label">Horario:</label>
					            <input type="text" required="required" name="time" class="time form-control">
					          </div>
					          <label class="control-label">Profissional:</label>
						          <select required="required" name="profissional" id="profess" class="select form-control">
						            	@foreach($profess as $prof)
					            		<option id="profess-{{$prof['id']}}" value="{{$prof['id']}}">{{$prof['data']['nome']}}</option>
						            	@endforeach
						            </select>
						          </div>
				          <div class="modal-footer">
				             <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				             <button type="submit" class="btn btn-info">Salvar</button>
				          </div>
				        </form>
				      </div> <!-- /.modal-content -->
				    </div> <!-- /.modal-dialog -->
				  </div> <!-- /.modal -->
	@endsection