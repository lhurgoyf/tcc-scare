<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="/material/assets/img/apple-icon.png">
	<link rel="icon" type="image/png" href="/material/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Login Screen</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />

	<!--     Fonts and icons     -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />

	<!-- CSS Files -->
    <link href="/material/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/material/assets/css/material-kit.css" rel="stylesheet"/>

</head>

<body class="signup-page">
	<nav class="navbar navbar-transparent navbar-absolute">
    	<div class="container">
        	<!-- Brand and toggle get grouped for better mobile display -->
        	<div class="navbar-header">
        		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example">
            		<span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
        		</button>
        	</div>
    	</div>
    </nav>
    <div class="wrapper">
		<div class="header header-filter" style="background-image: url('/material/assets/img/city.jpg'); background-size: cover; background-position: top center;">
			<div class="container">
				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-sm-2 col-sm-offset-2">
						<div class="card card-signup">
						<form action="/registro" method="POST">
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
								<div class="header header-info text-center">
									<h4>Registro</h4>
								</div>
								<div class="content">
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons"></i>
										</span>
										<input  type="text" required="required" class="form-control" name="name" placeholder="Nome..." maxlength="45" pattern="[A-Za-z\s].{5}[A-Za-z\s]+$">
									</div>
								</div>
								</div>
								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons"></i>
										</span>
										<input type="email" required="required" class="form-control" name="login" placeholder="E-mail...">
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<i class="material-icons"></i>
										</span>
										<input type="password" required="required" class="form-control" name="senha" placeholder="Senha...">		
									</div>
									<!-- If you want to add a checkbox to this form, uncomment this code

									<div class="checkbox">
										<label>
											<input type="checkbox" name="optionsCheckboxes" checked>
											Subscribe to newsletter
										</label>
									</div> -->
								</div>
								<div class="footer text-center">
									<button type="submit" class="btn btn-simple btn-primary btn-lg">Registrar</button>
									<a href="/login" class="btn btn-simple btn-primary btn-lg">Cancelar</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>


</body>
	<!--   Core JS Files   -->
	<script src="/material/assets/js/jquery.min.js" type="text/javascript"></script>
	<script src="/material/assets/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="/material/assets/js/material.min.js"></script>

	<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
	<script src="/material/assets/js/nouislider.min.js" type="text/javascript"></script>

	<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
	<script src="/material/assets/js/bootstrap-datepicker.js" type="text/javascript"></script>

	<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
	<script src="/material/assets/js/material-kit.js" type="text/javascript"></script>
	<script src="/js/jquery.mask.min.js" type="text/javascript"></script>
    <script src="/js/jquery.mask.js" type="text/javascript"></script>

</html>
