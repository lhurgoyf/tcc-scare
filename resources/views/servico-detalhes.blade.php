@extends('layouts.template')
@section('content')
 <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main"> 
	<div class="col-lg-12">
		<h1 class="page-header"></h1>
	</div>          
    <div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<form action="/servico/update" method="POST">
				<input type="hidden" name="id" value="{{$detail['id']}}" />
					<div class="panel-body">
						<div class="col-md-6">
						    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
						    <input type="hidden" name="id_empresa" value="1" />
							<div class="form-group label-floating">
								<label class="control-label">Tipo de Serviço:</label>
								<input class="form-control" name="tipo" value="{{$detail['tipo']}}" maxlength="45"  pattern="[A-Za-z\s].{2}[A-Za-z\s]+$">
							</div>				
							<div class="form-group label-floating">
								<label class="control-label">Valor:</label>
								<input class="money form-control" name="valor" value="{{$detail['valor']}}">
							</div>
						</div>
						<div class="col-md-12 widget-right">
						<a href="/servicos" class="btn btn-default pull-right">
										Cancelar
									</a>
							<button type="submit" class="btn btn-info pull-right">Salvar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endsection

  