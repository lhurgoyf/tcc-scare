@extends('layouts.template')
@section('content')
<script type="text/javascript">
	$(document).ready(function(){
		$('.search').keyup(function(){
			var value = $(this).val()
			$.ajax({
				type: 'GET',
				url: '/servicos/busca/' + value,
				dataType: 'JSON',
				success: function (data) {
					$('.result').html('')
					$.each(data, function(index, value) {
						$('.result').append('<tr><td>' + value.tipo + '</td><td>' + value.valor + '</td><td><a href="/servicos/delete/' + value.id + '" class="btn btn-danger">Excluir</a><a href="/servicos/edit/' + value.id + '" class="btn btn-default">Editar</a></td></tr>')
					})	
				}
			})
		})
			$('.del-btn').click(function() {
			var id = $(this).attr('id').split('-')[1];
			$('#final-del').attr('href', '/servico/delete/' + id);
	})
})
</script>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"></h1>
		</div>
	</div> <!--/.row-->
	<div class="row">
		@if(session('status'))
		<div class="col-lg-12">
			<div class="alert alert-success">{{session('status')}}</div>
		</div>
		@endif
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
				<div class="row">
							<div class="col-lg-6" style="margin-top:-11px; margin-left: -15px;">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="material-icons">search</i>
									</span>
									<input type="text" class="form-control search" placeholder="Nome do serviço">
								</div>
							</div>
							<div class="col-lg-6">
								<a href="newservicos"><button type="submit" class="btn btn-info pull-right">Novo Serviço</button></a>
							</div>
						</div>
						<br/><br/>
					<table class="table">
					    <thead>
						    <tr>
						        <th>Tipo</th>
						        <th>Valor</th>
						        <th></th>
						        <th></th>
						        <th>Ações</th>
						    </tr>
				    	</thead>
				    	<tbody class="result">
				    		@foreach($servicos as $ser)
				    		<tr>
				    			<td>{{$ser->tipo}}</td>
				    			<td>R$ {{$ser->valor,00}}</td>
				    			<td></td>
				    			<td></td>
				    		<td>
			    				<a href="#appear" id="delete-{{$ser->id}}" class="btn btn-danger del-btn" data-toggle="modal">Excluir</a>
			    				<a href="/servico/edit/{{$ser->id}}" class="btn btn-default">Editar</a>
			    			</td>
				    		</tr>
				    		@endforeach
				    	</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
  <div class="modal fade" id="appear" role="dialog">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header" style="padding:15px 30px;">
	      <button type="button" class="close" data-dismiss="modal">&times;</button>
	    </div>
	    <div class="modal-body" style="padding:40px 50px;">
	      <form role="form">
	          <label><h4>Tem certeza que deseja excluir esse serviço?</h4></label>
	        <div class="form-group">
	        </div>
	        <a href="#" id="final-del" class="btn btn-danger">Excluir</a>
	        <button class="btn btn-default" data-dismiss="modal">Cancelar</button>
	      </form>
		</div>
	  </div>
	</div>
  </div>
</div>
@endsection
