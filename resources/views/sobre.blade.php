@extends('layouts.template')
@section('content')		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
		<div class="row">
		<br>
			<div class="col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading"><svg class="glyph stroked email"><use xlink:href="#stroked-email"></use></svg> Formulario para Contato</div>
					<br><br><div class="panel-body">
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								<!-- Name input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="name">Nome</label>
									<div class="col-md-6">
									<input id="name" name="name" type="text" placeholder="Seu Nome" class="form-control">
									</div>
								</div>
							
								<!-- Email input-->
								<div class="form-group">
									<label class="col-md-3 control-label" for="email">Seu E-mail</label>
									<div class="col-md-6">
										<input id="email" name="email" type="text" placeholder="Seu e-mail" class="form-control">
									</div>
								</div>
								
								<!-- Message body -->
								<div class="form-group">
									<label class="col-md-3 control-label" for="message">Sua Mensagem</label>
									<div class="col-md-6">
										<textarea class="form-control" id="message" name="message" placeholder="Por favor coloque sua mensagem aqui..." rows="5"></textarea>
									</div>
								</div>
								
								<!-- Form actions -->
								<div class="form-group">
									<div class="col-md-12 widget-right">
										<button type="submit" class="btn btn-info pull-right">Enviar</button>
									</div>
								</div>
							</fieldset>
						</form>
					
					</div>

				</div>
			
	
@endsection