<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>S-care 1.0</title>
    <!-- Calendar links -->
    <link rel='stylesheet' href='/calendar/lib/cupertino/jquery-ui.min.css' />
    <link href='/calendar/fullcalendar.css' rel='stylesheet' />
    <link href='/calendar/fullcalendar.print.css' rel='stylesheet' media='print' />
    <!-- Material Fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />
    <!-- select2 -->
    <link rel="stylesheet" href="/select2/dist/css/select2.min.css" />
    <!-- Material CSS Files -->
    <link href="/material/assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/material/assets/css/material-kit.css" rel="stylesheet"/>   
    <!-- Styles -->
    <link href="/css/custom.css" rel="stylesheet">
    <link href="/css/styles.css" rel="stylesheet">
    <!-- Calendar JS Files -->
    <script src='/calendar/lib/jquery.min.js'></script>
    <script src='/calendar/lib/moment.min.js'></script>
    <script src='/calendar/fullcalendar.min.js'></script>
    <script src='/calendar/pt-br.js'></script>
    <script src="/js/lumino.glyphs.js"></script>
    <!-- Mascaras JS-->
    <script src="/js/jquery.mask.min.js" type="text/javascript"></script>
    <script src="/js/jquery.mask.js" type="text/javascript"></script>
    <script src="/js/custom.js"></script>
    <!-- Material JS -->
    <script src="/material/assets/js/bootstrap.min.js"></script>
    <script src="/material/assets/js/material.min.js"></script>
    <script src="/material/assets/js/material-kit.js"></script>
    <!-- select2 -->
    <script src="/select2/dist/js/select2.min.js"></script>
</head>
<!-- Dropdown Navbar Superior-->
<body>
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="agenda"><span>S</span>-Care</a>
                <ul class="user-menu">
                    <li class="dropdown pull-right">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> {{ Session::get('logadinho')['name'] }} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#" data-toggle="modal" data-target="#user-profile"><svg class="glyph stroked male-user"><use xlink:href="#stroked-male-user"></use></svg> Meus dados</a></li>
                            <li><a href="/login/leave"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Sair</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
<!-- Sidebar Esquerdo-->
    <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <br>
        <?php $route = Route::getCurrentRoute()->getPath(); ?>
        <ul class="nav menu nav-pills nav-pills-primary" role="tablist">
            <li class="<?=(preg_match_all('#(agenda)#is', $route)) ? 'active' : '' ?>"><a href="/agenda"><svg class="glyph stroked calendar"><use xlink:href="#stroked-calendar"></use></svg> Agenda </a></li>
            <li class="<?=(preg_match_all('#(clientes)#is', $route)) ? 'active' : '' ?>"><a href="/clientes"><svg class="glyph stroked male user "><use xlink:href="#stroked-male-user"/></use></svg> Clientes </a></li>
            <li class="<?=(preg_match_all('#(caixa)#is', $route)) ? 'active' : '' ?>"><a href="/caixa"><svg class="glyph stroked lock"><use xlink:href="#stroked-lock"/></use></svg> Caixa </a></li>
            <li class="<?=(preg_match_all('#(servicos)#is', $route)) ? 'active' : '' ?>"><a href="/servicos"><svg class="glyph stroked flag"><use xlink:href="#stroked-flag"/></use></svg>
            </svg> Serviços </a></li>
            <li class="<?=(preg_match_all('#(profissionais)#is', $route)) ? 'active' : '' ?>"><a href="/profissionais"><svg class="glyph stroked star"><use xlink:href="#stroked-star"></use></svg> Profissionais </a></li>
            <li role="presentation" class="divider"></li>
            <li><a href="/login/leave"><svg class="glyph stroked cancel"><use xlink:href="#stroked-cancel"></use></svg> Logoff</a></li>
        </ul>
    </div>
    @yield('content')
    <div class="modal fade" id="user-profile" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form action="/usuarios/save" method="POST">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Meus dados</h4>
          </div>
          <div class="modal-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <input type="hidden" name="id" value="{{Session::get('logadinho')['id']}}" />
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group label-floating">
                          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            <label class="control-label">Nome</label>
                            <input class="form-control" name="name" value="{{Session::get('logadinho')['name']}}"  maxlength="45"  pattern="[A-Za-z\s].{5}[A-Za-z\s]+$">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group label-floating">
                            <label class="control-label">Email (login)</label>
                            <input class="form-control" name="login" value="{{Session::get('logadinho')['login']}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Senha</label>
                            <input type="password" class="form-control" name="senha" placeholder="******">
                        </div>
                    </div>
                </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Salvar</button>
          </div>
        </form>
        </div>
      </div>
    </div>
</body>
</html>
