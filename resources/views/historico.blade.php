@extends('layouts.template')
@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">			
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"></h1>
		</div>
	</div> <!--/.row-->
	<div class="row">
    	@if(session('status'))
		<div class="col-lg-12">
			<div class="alert alert-success">{{session('status')}}</div>
		</div>
		@endif
		<div class="col-lg-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-6 pull-left">
								<h4>Caixa do dia - {{$date}}</h4>
							</div>
							<div class="col-md-4 pull-right">
								<form action="/caixa/historico" method="POST">
									<input type="hidden" name="_token" value="{{ csrf_token() }}" />
									<div class="input-group" style="margin-top:-25px; margin-left: -15px;">
										<span class="input-group-addon">
											<i class="material-icons">search</i>
										</span>
										<input type="text" name="date" class="form-control search" placeholder="Buscar no histórico por data">
									</div>
								</form>
							</div>
						</div>
						<hr/>
						<table class="table">
							<thead>
								<tr>
									<th>Cliente</th>
									<th>Servi&ccedil;o</th>
									<th>Total</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								@foreach($today as $box)
								<tr>
									<td>{{unserialize($box->data)['nome']}}</td>
									<td>
										@foreach($box['services'] as $ser)
										{{$ser->tipo}} - R${{number_format($ser->valor, 2, '.', '')}}<br/>
										@endforeach
									</td>
									<td><strong>R${{number_format($box['total'], 2, '.', '')}}</strong></td>
									<td><label class="label {{$box->status == 0 ? 'label-danger' : 'label-success' }}">{{$box->status == 0 ? 'Pendente' : 'Pago' }}</label></td>
								</tr>
								@endforeach
							</tbody>
						</table>
						<hr/>
						<h3 class="pull-right">Total do dia: R$<strong>{{$totalDay}}</strong></h3>
					</div>
			  </div>
		</div> 	
	</div> 
</div>
@endsection