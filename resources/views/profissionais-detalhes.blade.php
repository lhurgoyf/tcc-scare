@extends('layouts.template')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	    <div class="col-lg-12">
			<h1 class="page-header"></h1>
		</div>              
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
				 	<form action="/profissional/update"  method="POST">
						<input type="hidden" name="id" value="{{$details['id']}}" />
						<div class="panel-body">
							<div class="col-md-6">
								<div class="form-group label-floating">
						          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
									<label class="control-label">Nome</label>
									<input type="text" class="form-control" name="nome" value="{{$details['details']['nome']}}"  maxlength="45"  pattern="[A-Za-z\s].{5}[A-Za-z\s]+$">
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Telefone</label>
									<input class="phone_with_ddd form-control" name="telefone" value="{{$details['details']['telefone']}}">
								</div>
								
								<div class="form-group label-floating">
									<label class="control-label">E-mail</label>
									<input type="email" id='email' class="form-control" name="email" value="{{$details['details']['email']}}">
								</div>
							</div>
							<div class="col-md-3">
									<div class="form-group label-floating">
										<label class="control-label">CPF</label>
										<input type="text" required class="cpf form-control" name="cpf" value="{{$details['details']['cpf']}}">
									</div>									
									<div class="form-group">
										<label>Sexo</label>
										<select type="text" required class="form-control" name ="servico" value="{{$details['details']['email']}}">
											<option>Masculino</option>
											<option>Feminino</option>
										</select>
									</div>
							</div>
						<div class="col-md-12 widget-right">
						<a href="/profissionais" class="btn btn-default pull-right">
										Cancelar
									</a>
							<button type="submit" class="btn btn-info pull-right">
								Salvar
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection

