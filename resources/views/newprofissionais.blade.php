@extends('layouts.template')
@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    	<div class="col-lg-12">
				<h1 class="page-header"></h1>
			</div>              
        <div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
						<div class="panel-body">
							<form action="/newprofissionais" method="POST">
								<div class="col-md-6">
									<div class="form-group label-floating">
							          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
										<label class="control-label">Nome:</label>
										<input type="text" required="required" class="form-control" name="nome" maxlength="45" pattern="[A-Za-z\s].{5}[A-Za-z\s]+$">
									</div>
									<div class="form-group label-floating">
										<label class="control-label">Telefone:</label>
										<input type="text" required="required"class="phone_with_ddd form-control" name="telefone">
									</div>
									<div class="form-group label-floating">
										<label class="control-label">E-mail:</label>
										<input type="email" required="required" id='email' class="email form-control" name="email">
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group label-floating">
										<label class="control-label">CPF</label>
										<input type="text" required="required" class="cpf form-control" name="cpf">
									</div>									
									<div class="form-group">
										<label>Sexo</label>
										<select type="text" required class="form-control" name ="sexo">
											<option>Masculino</option>
											<option>Feminino</option>
										</select>
									</div>
								</div>
								<div class="col-md-12 widget-right">
								<a href="/profissionais" class="btn btn-default pull-right">
										Cancelar
									</a>
									<button type="submit" class="btn btn-info pull-right">
										Salvar
									</button>
								</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection

  