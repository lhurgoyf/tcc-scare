-- MySQL dump 10.13  Distrib 5.6.28, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: lummin
-- ------------------------------------------------------
-- Server version	5.6.28-0ubuntu0.15.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agenda`
--

DROP TABLE IF EXISTS `agenda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `agenda` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `servicos` longtext NOT NULL,
  `date` datetime NOT NULL,
  `profissional` int(11) NOT NULL,
  `created_at` varchar(45) DEFAULT NULL,
  `updated_at` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_idx` (`id_cliente`),
  CONSTRAINT `fk_agenda_1` FOREIGN KEY (`id_cliente`) REFERENCES `clientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agenda`
--

LOCK TABLES `agenda` WRITE;
/*!40000 ALTER TABLE `agenda` DISABLE KEYS */;
INSERT INTO `agenda` VALUES (1,20,'a:3:{i:0;s:2:\"44\";i:1;s:2:\"46\";i:2;s:2:\"48\";}','2016-12-07 12:34:00',11,'2016-12-03 01:10:45','2016-12-03 01:10:45'),(3,21,'a:1:{i:0;s:2:\"46\";}','2016-12-15 11:11:00',11,'2016-12-03 01:44:07','2016-12-03 01:44:07');
/*!40000 ALTER TABLE `agenda` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `caixa`
--

DROP TABLE IF EXISTS `caixa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `caixa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `servicos` varchar(255) NOT NULL,
  `pagamento` varchar(45) NOT NULL,
  `desconto` varchar(45) NOT NULL,
  `total` int(11) NOT NULL,
  `data` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_caixa_1_idx` (`id_empresa`),
  KEY `fk_caixa_2_idx` (`servicos`),
  CONSTRAINT `fk_caixa_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `caixa`
--

LOCK TABLES `caixa` WRITE;
/*!40000 ALTER TABLE `caixa` DISABLE KEYS */;
/*!40000 ALTER TABLE `caixa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_clientes_1_idx` (`id_empresa`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (20,1,'a:5:{s:4:\"nome\";s:9:\"Something\";s:8:\"telefone\";s:14:\"(66) 6666-6666\";s:5:\"email\";s:13:\"666@gmail.com\";s:3:\"cpf\";s:14:\"042.950.731.30\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-11-25 20:21:10','2016-11-25 20:21:10'),(21,1,'a:5:{s:4:\"nome\";s:10:\"Joana Darc\";s:8:\"telefone\";s:14:\"(61) 9202-9136\";s:5:\"email\";s:26:\"lucas.freitas@spymypet.com\";s:3:\"cpf\";s:14:\"766.563.491.53\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-01 15:36:14','2016-12-01 15:36:14'),(22,1,'a:5:{s:4:\"nome\";s:22:\"Lucas de Souza Freitas\";s:8:\"telefone\";s:14:\"(61) 8678-0870\";s:5:\"email\";s:17:\"lucassir@live.com\";s:3:\"cpf\";s:14:\"766.563.491.53\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:47:52','2016-12-02 22:47:52'),(23,1,'a:5:{s:4:\"nome\";s:29:\"angela rosa epifanio de souza\";s:8:\"telefone\";s:14:\"(61) 9202-9136\";s:5:\"email\";s:17:\"lucassir@live.com\";s:3:\"cpf\";s:14:\"766.563.491.53\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:50:12','2016-12-02 22:50:12'),(24,1,'a:5:{s:4:\"nome\";s:14:\"Lucas de Souza\";s:8:\"telefone\";s:14:\"(61) 9202-9136\";s:5:\"email\";s:17:\"lucassir@live.com\";s:3:\"cpf\";s:14:\"766.563.491.53\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:51:28','2016-12-02 22:51:28'),(25,1,'a:5:{s:4:\"nome\";s:29:\"angela rosa epifanio de souza\";s:8:\"telefone\";s:14:\"(61) 9202-9136\";s:5:\"email\";s:17:\"lucassir@live.com\";s:3:\"cpf\";s:14:\"766.563.491.53\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:52:25','2016-12-02 22:52:25');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empresa`
--

DROP TABLE IF EXISTS `empresa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dataempresa` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empresa`
--

LOCK TABLES `empresa` WRITE;
/*!40000 ALTER TABLE `empresa` DISABLE KEYS */;
INSERT INTO `empresa` VALUES (1,'Empresa 1'),(2,'Empresa 2');
/*!40000 ALTER TABLE `empresa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profissionais`
--

DROP TABLE IF EXISTS `profissionais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `profissionais` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `data` longtext NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_empresa_idx` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profissionais`
--

LOCK TABLES `profissionais` WRITE;
/*!40000 ALTER TABLE `profissionais` DISABLE KEYS */;
INSERT INTO `profissionais` VALUES (11,1,'a:5:{s:4:\"nome\";s:11:\"Bruno Costa\";s:8:\"telefone\";s:14:\"(61) 9202-9136\";s:5:\"email\";s:23:\"bcostabatista@gmail.com\";s:3:\"cpf\";s:14:\"035.282.711.45\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-11-25 20:32:57','2016-11-25 20:32:57'),(12,1,'a:5:{s:4:\"nome\";s:18:\"Profissional Teste\";s:8:\"telefone\";s:14:\"(55) 5555-5555\";s:5:\"email\";s:15:\"email@gmail.com\";s:3:\"cpf\";s:14:\"555.555.555.55\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:57:25','2016-12-02 22:57:25'),(13,2,'a:5:{s:4:\"nome\";s:22:\"Profissional da empres\";s:8:\"telefone\";s:14:\"(88) 8888-8888\";s:5:\"email\";s:22:\"profissional@gmail.com\";s:3:\"cpf\";s:14:\"999.999.999.99\";s:4:\"sexo\";s:9:\"Masculino\";}','2016-12-02 22:58:59','2016-12-02 22:58:59');
/*!40000 ALTER TABLE `profissionais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_empresa` int(11) NOT NULL,
  `tipo` varchar(45) NOT NULL,
  `valor` double NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (44,1,'Testando',15.5,'2016-11-25 20:31:39','2016-11-25 20:31:39'),(46,1,'Over The Rainbow',250,'2016-11-25 20:52:45','2016-11-25 20:52:45'),(47,2,'Manicure',30,'2016-12-02 22:48:58','2016-12-02 22:48:58'),(48,1,'Manicure',1.234,'2016-12-02 22:51:05','2016-12-02 22:51:05');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `login` varchar(45) NOT NULL,
  `senha` varchar(45) NOT NULL,
  `id_empresa` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_usuarios_1_idx` (`id_empresa`),
  CONSTRAINT `fk_usuarios_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Chance Cassin','furman.turner@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:35','2016-12-03 00:52:35'),(2,'Eula Abbott','auer.hope@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:35','2016-12-03 00:52:35'),(3,'Eleanore Ernser I','aosinski@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:35','2016-12-03 00:52:35'),(4,'Mrs. Lonie Blanda I','trevion82@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(5,'Romaine Kuvalis','aufderhar.genesis@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(6,'Adrian Miller','bill.abbott@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(7,'Camille Kautzer III','tatum.breitenberg@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(8,'Florine Gislason','howe.manley@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(9,'Darryl Swift','kaci.smith@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(10,'Fatima Carter','ewindler@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(11,'Ms. Trisha Goldner Jr.','herminia92@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(12,'Prof. Billy McLaughlin Jr.','yconnelly@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(13,'Walker Mitchell','ernser.ruthie@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(14,'Ms. Jazlyn Carter MD','adonis.damore@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(15,'Dr. Garth Hettinger I','tiffany94@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(16,'Yazmin Jones','ratke.kenyon@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(17,'Lyric Williamson','daryl.denesik@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(18,'Furman Trantow','windler.cydney@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(19,'Kayleigh Johns','tristin.parker@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(20,'Prof. Eldon Osinski','luettgen.ricardo@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(21,'Bart Mayert','salma55@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(22,'Savannah Hodkiewicz','durgan.caesar@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(23,'Vaughn Toy PhD','xgoyette@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(24,'Deron Dicki Sr.','schaden.ernest@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:36','2016-12-03 00:52:36'),(25,'Mrs. Adeline Rohan','lilly.hammes@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(26,'Aurelio Terry','pasquale20@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(27,'Lacey Thiel','beier.chesley@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(28,'Grace Donnelly','lind.jo@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(29,'Prof. Alison Lind V','mroberts@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(30,'Ms. Isabell Reichel','wilderman.eugenia@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(31,'Brooks Mayer','melisa55@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(32,'Theodore Fay','erin74@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(33,'Brady Larson','goldner.akeem@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(34,'Kenton Stoltenberg','jay.bechtelar@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(35,'Buford Hilll','lavina76@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(36,'Brennan Turcotte MD','makayla60@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(37,'Mrs. Alayna Reichel','ifunk@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(38,'Josefa Fisher','iferry@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(39,'Dr. Hanna Reynolds','ireinger@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(40,'Felipa Frami','whudson@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(41,'Bailee Bechtelar','alf.king@example.org','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(42,'Devonte Ernser','mhand@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(43,'Alicia Skiles','coralie.wisoky@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(44,'Dr. Barrett Bednar','stroman.rocio@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(45,'Lorine Weber','nswift@example.net','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(46,'Craig Murray','ledner.tamia@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(47,'Elnora Gulgowski','mya.crona@example.org','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(48,'Ms. Alison Ledner','mfeeney@example.net','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(49,'Mackenzie Wiza III','mavis.hickle@example.com','e10adc3949ba59abbe56e057f20f883e',2,'2016-12-03 00:52:37','2016-12-03 00:52:37'),(50,'Emmie Kulas','berneice.rolfson@example.com','e10adc3949ba59abbe56e057f20f883e',1,'2016-12-03 00:52:37','2016-12-03 00:52:37');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-03  1:58:29
