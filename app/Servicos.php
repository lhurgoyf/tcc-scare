<?php

namespace App;

class Servicos extends \Eloquent
{
   public $table = 'servicos';
   
   protected $fillable = [
	 'id_empresa',
	 'tipo',
	 'valor'
	];
}
