<?php

namespace App;

class Usuarios extends \Eloquent
{
   public $table = 'usuarios';
   
   protected $fillable = [
    'name',
    'login',
    'senha'
    ];
}
