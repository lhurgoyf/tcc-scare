<?php

namespace App;

class Caixa extends \Eloquent
{
   public $table = 'caixa';
   
   protected $fillable = [
    'id_cliente'
    'servico'
    'data'
    'horario'
   ];
}
