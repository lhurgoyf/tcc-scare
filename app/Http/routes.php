<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['auth']], function () {
    Route::get('/', 'AgendaController@getAgenda');
	Route::get('/agenda', 'AgendaController@getAgenda');
	Route::post('/agenda', 'AgendaController@create');
	Route::get('/agenda/{id}', 'AgendaController@getEvent');
	Route::get('/agenda/delete/{id}', 'AgendaController@delete');
	Route::post('/agenda/update/data', 'AgendaController@update');

	Route::get('/clientes', 'ClientesController@getClientes');
	Route::get('/cliente/delete/{id}', 'ClientesController@delete')
	->where('id', '[0-9]+');
	Route::get('/cliente/edit/{id}', 'ClientesController@getCliente')
	->where('id', '[0-9]+');
	Route::get('/clientes/busca/{value?}', 'ClientesController@search');
	Route::post('/cliente/update', 'ClientesController@update');
	Route::get('/newclientes', 'ClientesController@getNewclientes');
	Route::post('/newclientes', 'ClientesController@create');

	Route::get('/caixa', 'CaixaController@getCaixa');
	Route::get('/caixa/pay/{id}', 'CaixaController@pay');
	Route::post('/caixa/historico', 'CaixaController@history');

	Route::get('/profissionais', 'ProfissionaisController@getProfissionais');
	Route::get('/profissional/delete/{id}', 'ProfissionaisController@delete')
	->where('id', '[0-9]+');
	Route::get('/profissional/edit/{id}', 'ProfissionaisController@getProfissional')
	->where('id', '[0-9]+');
	Route::get('/profissionais/busca/{value?}', 'ProfissionaisController@search');
	Route::post('/profissional/update', 'ProfissionaisController@update');
	Route::get('/newprofissionais', 'ProfissionaisController@getNewprofissionais');
	Route::post('/newprofissionais', 'ProfissionaisController@create');

	Route::post('/usuarios/save', 'LoginController@save');

	Route::post('/newservicos', 'ServicosController@create');
	Route::post('/servico/update', 'ServicosController@update');
	Route::get('/servico/delete/{id}', 'ServicosController@delete')
	->where('id', '[0-9]+');
	Route::get('/servico/edit/{id}', 'ServicosController@getServico')
	->where('id', '[0-9]+');
	Route::get('/servicos/busca/{value?}', 'ServicosController@search');
	Route::get('/servicos', 'ServicosController@getServicos');
	Route::get('/newservicos', 'ServicosController@getNewservicos');

	Route::get('/historico', 'CaixaController@getHistorico');
	Route::get('/login/leave', 'LoginController@signOut');
	Route::post('/usuarios/save', 'LoginController@save');
});
Route::get('/registro', 'LoginController@getRegistro');
Route::post('/registro', 'LoginController@create');
Route::get('/login', 'LoginController@getLogin');
Route::post('/login', 'LoginController@login');

