<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;

class ServicosController extends BaseController
{
	private $_user;

	public function __construct() {
		$this->_user = \Session::get('logadinho')['id_empresa'];
	}

	public function getServicos()
	{
		$servicos = \App\Servicos::where('id_empresa', $this->_user)->get();
		return view('servicos', ['servicos' => $servicos]);
	}
	public function getNewservicos()
	{
		return view('newservicos');
	}	
	
	public function getServico($id)
	{
		$servico = \App\Servicos::find($id);
		return view('servico-detalhes', ['detail'=> $servico]);
	}

	public function create(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$servicos = new \App\Servicos;
		foreach($params as $key => $value) {
			$servicos[$key] = $value;

		}
		$servicos->save();
		return \Redirect::to('/servicos')->with('status', 'Cadastro realizado com sucesso');;
	}
	public function update(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$id = $params['id'];
		unset($params['id']);
		$servicos = \App\Servicos::find($id);
		foreach($params as $key => $value){
			$servicos[$key] = $value;
		}
		$servicos->save();
		return \Redirect::to('/servicos')->with('status', 'Cadastro atualizado com sucesso');
	}

	public function delete($id) 
	{
		\App\Servicos::destroy($id);
		return \Redirect::to('/servicos');
	}
	public function search($value = null) 
	{
		$services = new \App\Servicos;

		$res = $services->where('id_empresa', $this->_user)
						->where('tipo', 'LIKE', '%' . $value . '%')
						->get();
		
		echo json_encode($res);	
	}

}
