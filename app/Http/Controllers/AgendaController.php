<?php
//namespace de referencia as funções da class App 
namespace App\Http\Controllers;
//namespaces de referencia as funções da class Illuminate
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;

class AgendaController extends BaseController
{
	private $_user;

	public function __construct() {
		$this->_user = \Session::get('logadinho')['id_empresa'];
	}

	public function getAgenda() {
		$user = \Session::get('logadinho')['id'];
		$sched = new \App\Agenda;
		$data = $sched->leftjoin('clientes', 'agenda.id_cliente', '=', 'clientes.id')
					  ->where('clientes.id_empresa', $this->_user)
					  ->select(['agenda.id AS agenda_id', 'agenda.*', 'clientes.*'])
					  ->get();
		$customers = \App\Clientes::where('id_empresa', $this->_user)->get();
		$services = \App\Servicos::where('id_empresa', $this->_user)->get();
		$profess = \App\Profissionais::where('id_empresa', $this->_user)->get();
		$arr = [];
		$arrx = [];
		foreach($customers as $custom) {
			$new['id'] = $custom['id'];
			$new['data'] = unserialize($custom['data']);
			$arr[] = $new;
		}
		foreach($profess as $prof) {
			$newest['id'] = $prof['id'];
			$newest['data'] = unserialize($prof['data']);
			$arrx[] = $newest;

		}
		return view('agenda', ['agendas' => $data, 'customers' => $arr, 'services' => $services, 'profess' => $arrx]);
   	}
   	
	public function create(Request $request) {
		$params = $request->all();
		$services = serialize($params['servico']);
		$time = $params['time'];
		unset($params['_token'], $params['time'], $params['servico']);
		$params['servicos'] = $services;
		$agenda = new \App\Agenda;
		foreach($params as $key => $value) {
			$agenda[$key] = $value;
			if($key === 'date') {
				$agenda[$key] = implode('-', array_reverse(explode('/', $value))) . ' ' . $time;
			}
			continue;
		}
		$agenda->save();
		return \Redirect::to('/agenda');
	}

	public function getEvent($id) {
		$item = \App\Agenda::find($id);
		$ids = unserialize($item['servicos']);
		$services = [];
		foreach($ids as $id) {
			$serv = \App\Servicos::find($id);
			$services[] = $serv;
		}
		$item['services'] = $services;
		echo json_encode($item);
	}

	public function delete($id) 
	{	
		$agenda = \App\Agenda::destroy($id);
		return \Redirect::to('/agenda');
	}

	public function update(Request $request) {
		$params = $request->all();
		$agenda = \App\Agenda::find($params['id']);
		$agenda->id_cliente = $params['id_cliente'];
		$time = $params['time'];
		$agenda->servicos = serialize($params['servicos']);
		$agenda->date = implode('-', array_reverse(explode('/', $params['date']))) . ' ' . $params['time'];
		$agenda->profissional = $params['profissional'];
		$agenda->save();
		return \Redirect::to('/agenda')->with('status', 'Dados atualizados com sucesso');
	}
}
