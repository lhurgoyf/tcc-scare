<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class LoginController extends BaseController
{
    public function getLogin() {
		return view('login');
	}

	public function getRegistro() {
		return view('registro');
	}

	public function login(Request $request) {
		$params = $request->all();
		$user = \App\Usuarios::where([
			['login', '=', $params['login']]
		])->first();
		if($user['senha'] === md5($params['senha'])) {
			$logged = [
				'id' => $user['id'],
				'name' => $user['name'],
				'login' => $user['login'],
				'id_empresa' => $user['id_empresa']
			];
			session(['logadinho' => $logged]);
			return \Redirect::to('/agenda');
		}
		return \Redirect::to('/login')->with('status', 'Login ou Senha incorretos...');;
	}

	public function create(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$user = new \App\Usuarios;
		$find = $user->where('login', $params['login'])->get();
		if(count($find) >= 1) {
			return \Redirect::to('/login')->with('status', 'Este usuário já existe');
		}
		$user->name = $params['name'];
		$user->login = $params['login'];
		$user->senha = md5($params['senha']);
		$user->id_empresa = 1;

		$user->save();
		return \Redirect::to('/login')->with('state', 'Cadastro realizado com sucesso...');
	}
 
	public function signOut(Request $request) {
		$request->session()->forget('logadinho');
		return \Redirect::to('/login');
	}

	public function save(Request $request) {
		$params = $request->all();
		unset($params['_token']);
		$user = \App\Usuarios::find($params['id']);
		$user->name = $params['name'];
		$user->login = $params['login'];
		if($params['senha'] != '') {
			$user->senha = md5($params['senha']);
		}
		$user->save();
		return \Redirect::to('/agenda')->with('status', 'Dados atualizados com sucesso');
	}
}
