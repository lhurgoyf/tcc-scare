<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CaixaController extends BaseController
{
	private $_user;

	public function __construct() {
		$this->_user = \Session::get('logadinho')['id_empresa'];
	}

	public function getCaixa() {
		$sched = new \App\Agenda;
		$data = $sched->leftjoin('clientes', 'agenda.id_cliente', '=', 'clientes.id')
					  ->where('clientes.id_empresa', $this->_user)
					  ->select(['agenda.id AS agenda_id', 'agenda.*', 'clientes.*'])
					  ->get();
		$today = date('Y-m-d');
		$all = [];
		$totalDay = null;
		foreach($data as $ser) {
			$mount = [];
			$total = null;
			$ids = unserialize($ser['servicos']);
			foreach($ids as $id) {
				$service = \App\Servicos::find($id);
				$total += $service->valor;
				$mount[] = $service;
			}
			$ser['total'] = $total;
			$ser['services'] = $mount;
			$date = date('Y-m-d', strtotime($ser['date']));
			if($today === $date && $ser->status != 0) {
				$totalDay += $total;
			}
			if($today === $date) {
				$all[] = $ser;
			}
		}
		return view('caixa', ['today' => $all, 'totalDay' => $totalDay]);
	}

	public function history(Request $request) {
		$params = $request->all();
		$sched = new \App\Agenda;
		$data = $sched->leftjoin('clientes', 'agenda.id_cliente', '=', 'clientes.id')
					  ->where('clientes.id_empresa', $this->_user)
					  ->select(['agenda.id AS agenda_id', 'agenda.*', 'clientes.*'])
					  ->get();
		$search = $params['date'];
		$today = implode('-', array_reverse(explode('/', $params['date'])));
		$all = [];
		$totalDay = null;
		foreach($data as $ser) {
			$mount = [];
			$total = null;
			$ids = unserialize($ser['servicos']);
			foreach($ids as $id) {
				$service = \App\Servicos::find($id);
				$total += $service->valor;
				$mount[] = $service;
			}
			$ser['total'] = $total;
			$ser['services'] = $mount;
			$date = date('Y-m-d', strtotime($ser['date']));
			if($today === $date && $ser->status != 0) {
				$totalDay += $total;
			}
			if($today === $date) {
				$all[] = $ser;
			}
		}
		return view('historico', ['today' => $all, 'totalDay' => $totalDay, 'date' => $search]);
	}

	public function pay($id) {
		$sched = \App\Agenda::find($id);
		$sched->status = 1;
		$sched->save();
		return \Redirect::to('/caixa')->with('status', 'Pagamento efetuado');
	}
}
