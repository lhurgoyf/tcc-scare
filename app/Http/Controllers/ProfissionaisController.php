<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;

class ProfissionaisController extends BaseController
{
	private $_user;

	public function __construct() {
		$this->_user = \Session::get('logadinho')['id_empresa'];
	}

	public function getProfissionais()
	{
		$profissionais = \App\Profissionais::where('id_empresa', $this->_user)->get();
		$array = [];
		foreach($profissionais as $proph) {
		 $dado = unserialize($proph->data);
		 $dado['id'] = $proph->id;
		 $array[] = $dado;
		 	
		}
		return view('profissionais', ['profissionais' => $array]);
	}

	public function getNewprofissionais()
	{
		return view('newprofissionais');
	}	
	

	public function create(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$newprofissionais = new \App\Profissionais;
		$all = $newprofissionais->all();
		foreach($all as $data) {
			$customer = unserialize($data['data']);
			if($customer['email'] === $params['email'] || $customer['cpf'] === $params['cpf']) {
				return \Redirect::to('/profissionais')->with('status', 'Este cadastro já existe');
			}
		}
		$newprofissionais->id_empresa = $this->_user;
		$newprofissionais->data = serialize($params);
		$newprofissionais->save();
		return \Redirect::to('/profissionais')->with('status', 'Cadastro realizado com sucesso');;
	
	}

	public function getProfissional($id)
	{
		$profissional = \App\Profissionais::find($id);
		return view('profissionais-detalhes', ['details' => ['id' => $profissional->id, 'details' => unserialize($profissional->data)]]);

	}

	public function update(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$id = $params['id'];
		$profissionais = \App\Profissionais::find($id);
		$profissionais->data = serialize($params);
		$profissionais->save();
		return \Redirect::to('/profissionais')->with('status', 'Cadastro atualizado com sucesso');
	}

	public function delete($id) 
	{
		\App\Profissionais::destroy($id);
		return \Redirect::to('/profissionais');
	}
	
	public function search($value = null) {
			$res = \App\Profissionais::where('id_empresa', $this->_user)->get();
			$arr = [];
			foreach($res as $search) {
				$mount = unserialize($search->data);
				preg_match_all('#(' . $value . ')#is', $mount['nome'], $matches);
				if($matches[1]) {
					$customer = [
					    'id' => $search->id,
						'nome' => $mount['nome'],
						'email' => $mount['email'],
						'telefone' => $mount['telefone']
					];
					$arr[] = $customer;
				}
			}
			echo json_encode($arr);	
		}
}


