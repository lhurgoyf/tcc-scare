<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Http\Request;

class ClientesController extends BaseController 
{
	private $_user;

	public function __construct() {
		$this->_user = \Session::get('logadinho')['id_empresa'];
	}

	public function getClientes() 
	{
		$clientes = \App\Clientes::where('id_empresa', $this->_user)->get();
		$array = [];
		foreach($clientes as $client) {
		 $dado = unserialize($client->data);
		 $dado['id'] = $client->id;
		 $array[] = $dado;
		}
		return view('clientes', ['clientes' => $array]);
	}

	public function getNewclientes() 
	{
		return view('newclientes');
	}

	public function create(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$newclientes = new \App\Clientes;
		$all = $newclientes->all();
		foreach($all as $data) {
			$customer = unserialize($data['data']);
			if($customer['email'] === $params['email'] || $customer['cpf'] === $params['cpf']) {
				return \Redirect::to('/clientes')->with('status', 'Este cadastro já existe');
			}
		}
		$newclientes->id_empresa = 1;
		$newclientes->data = serialize($params);
		$newclientes->save();
		return \Redirect::to('/clientes')->with('status', 'Cadastro realizado com sucesso');
	}

	public function getCliente($id)
	{
		$cliente = \App\Clientes::find($id);
		return view('cliente-detalhes', ['detalhes' => ['id' => $cliente->id, 'detalhes' => unserialize($cliente->data)]]);
	}

	public function update(Request $request)
	{
		$params = $request->all();
		unset($params['_token']);
		$id = $params['id'];
		unset($params['id']);
		$clientes = \App\Clientes::find($id);
		$clientes->data = serialize($params);
		$clientes->save();
		return \Redirect::to('/clientes')->with('status', 'Cadastro atualizado com sucesso');
	}

	public function delete($id) 
	{
		\App\Clientes::destroy($id);
		return \Redirect::to('/clientes');
	}

	public function search($value = null) {
		$res = \App\Clientes::where('id_empresa', $this->_user)->get();
		$arr = [];
		foreach($res as $search) {
			$mount = unserialize($search->data);
			preg_match_all('#(' . $value . ')#is', $mount['nome'], $matches);
			if($matches[1]) {
				$customer = [
				    'id' => $search->id,
					'nome' => $mount['nome'],
					'email' => $mount['email'],
					'telefone' => $mount['telefone']
				];
				$arr[] = $customer;
			}
		}
		echo json_encode($arr);	
	}
}
