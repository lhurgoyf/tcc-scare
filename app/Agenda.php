<?php

namespace App;

class Agenda extends \Eloquent
{
   public $table = 'agenda';
   
   protected $fillable = [
   	'id_cliente',
   	'servico',
   	'data',
   	'profissional',
   ];
}
